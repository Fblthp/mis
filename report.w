\documentclass[12pt]{article}

\usepackage{ams math, ams symb}
\usepackage[T2A]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[russian]{babel}
\usepackage{yfonts}
\usepackage{graphicx}
\usepackage{physics}
\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{minted}
\usepackage{hyperref}
\usepackage{xcolor}
\usepackage{graphicx}
\usepackage{sidecap}
\usepackage{subfigure}
\usepackage{caption}
\usepackage[left=25mm, top=20mm, right=10mm, bottom=20mm, nohead, nofoot]{geometry}

\graphicspath{{./graphs/}}

\makeatletter
\makeatother

\DeclareGraphicsExtensions{.pdf,.png,.svg,.jpg}

\newcommand{\Red}[1]{\textcolor{black}{\textbf{#1}}}

\title{\textbf{Использование алгоритма Брона-Кербоша для поиска независимых множеств.}}
\author{Жихарева Александра, Щуров Алексей, Блинов Игорь}
\date{Б8403а}
\begin{document}
\maketitle

\subsection*{Постановка задачи}
\bigbreak
%\paragraph{}
Дана система линейных неравенств $Ax < b$, где $x = (x_1, ..., x_n), b = (b_1, ..., b_m)$.

\bigbreak
%\paragraph{}
Необходимо разбить ее на максимальные по размеру группы неравенств таким образом, чтобы в любой группе у любых двух неравенств не было общих переменных.

\bigbreak
%\paragraph{}
Более формально, пусть $G_1, \dots, G_q$ - группы линейных неравенств, $G_i \subseteq \{1, \dots, m\}, G_i \neq \varnothing, ~ \forall i, j, ~ i \neq j : G_i \cap G_j = \varnothing, ~ \cup_{1 \leq i \leq q} G_i = \{1, \dots, m\}$,  тогда $\forall G_i$, $ ~ \forall j, k, ~ j \neq k \in G_i, ~ \forall t : ~ A_{j, t} \neq 0 \implies A_{k, t} = 0$

\bigbreak
%\paragraph{}
Для решения данной задачи представим систему неравенств в виде неориентированного графа, такого, что все неравенства образуют множество вершин, а ребро между двумя вершинами существует в том случае, если у соответствующих неравенств есть хотя бы одна общая переменная.

\bigbreak
%\paragraph{}
Более формально, построим граф $G = (V, E)$, где $V = \{1 \dots m \}$, $E \subseteq V \times V$, $(u, v) \in E \iff \exists i : A_{u, i} \neq 0, A_{v, i} \neq 0$

\bigbreak
%\paragraph{}
Тогда для того, чтобы разбить исходную систему неравенств на группы, достаточно будет разложить построенный граф на независимые множества. Это можно сделать с помощью алгоритма Брона-Кербоша.

\bigbreak
\subsection*{Алгоритмика}

\bigbreak
%\paragraph{}
Алгоритм Брона-Кербоша — метод ветвей и границ для поиска всех клик неориентированного графа.

\bigbreak
%\paragraph{}
Алгоритм использует тот факт, что всякая клика в графе является его максимальным по включению полным подграфом. Начиная с одиночной вершины (образующей полный подграф), алгоритм на каждом шаге пытается увеличить уже построенный полный подграф (мн-во \textit{compsub}), добавляя в него вершины из множества кандидатов (\textit{candidates}). Высокая скорость обеспечивается отсечением при переборе вариантов, которые заведомо не приведут к построению клики, для чего используется дополнительное множество (\textit{wrong}), в которое помещаются вершины, которые уже были использованы для увеличения полного подграфа.\cite{description}

\bigbreak
%\paragraph{}
Для того, чтобы использовать этот алгоритм для поиска  максимальных по включению независимых множеств вершин, можно построить дополнение $G'$ исходного графа $G$ (такой граф $G' = (V', E')$, что $V' = V, (u, v) \in E' \iff (u, v) \notin E$) и искать в нём клики, либо, что равносильно, модифицировать условия поиска клик в исходном графе.

%\paragraph{}
Время работы алгоритма в худшем случае оценивается как $O(3^{\frac{N}{3}})$, где $N = |V|$.


\newpage
\subsection*{Реализация \cite{implementation}}
@d Check if all in wrong connected with smth in candidates @{for i in wrong:
    q = True
    for j in candidates:
        if m[i][j]:
            q = False
            break
    if q:
        return False
return True
@}
@d Initialize BK result with empty array @{results = []
@}
@d Remove vertex from candidates, compsub and wrong sets @{candidates.remove(v)
compsub.remove(v)
wrong.append(v)
@}
@d Define check @{def check(candidates, wrong):
    @<Check if all in wrong connected with smth in candidates@>
@}
@d Fill sets with those sets we described @{v = candidates[0]
compsub.append(v)
new_candidates = [i for i in candidates if not m[i][v] and i != v]
new_wrong = [i for i in wrong if not m[i][v] and i != v]
@}
@d Add vertex to compsub or call recursive @{if not new_candidates and not new_wrong:
    results.append(list(compsub))
else:
    extend(compsub, new_candidates, new_wrong)
@}
@d Define recursive procedure @{def extend(compsub, candidates, wrong):
    while candidates and check(candidates, wrong):
        @<Fill sets with those sets we described@>
        @<Add vertex to compsub or call recursive @>
        @<Remove vertex from candidates, compsub and wrong sets@>
@}
@d Run BK @{extend([], list(range(len(m))), [])
@}
@o src/source.py @{def bron_kerbosch_max_by_inclusion(m):
    @<Initialize BK result with empty array@>
    @<Define check@>
    @<Define recursive procedure@>
    @<Run BK@>
    return results
@<Define function, making graph from matrix@>
@}

Функция $\textit{make\_graph}$ cтроит граф из системы по описанному правилу.
@d Define function, making graph from matrix@{
def make_graph(mat):
    n = len(mat)
    m = len(mat[0])
    result = [[False for j in range(n)] for i in range(n)]
    for j in range(m):
        adj = []
        for i in range(n):
            if mat[i][j] != 0:
                for k in adj:
                    result[i][k] = result[k][i] = True
                adj.append(i)
    return result
@}

\bigbreak
Работа алгоритма начинается с вызова процедуры \textit{extend} от пустого множества \textit{compsub} (с самого начала подграф пуст), множества \textit{candidates}, состоящего из всех вершин графа (можно начать составление подграфа с любого из кандидатов), и пустого множества \textit{wrong}, которое будет содержать вершины, уже использованные на текущем шаге.

Каждый шаг алгоритма может быть одним из двух действий. Переход к следующей итерации, сопровождающийся удалением текущей вершины из множества кандидатов и текущего подграфа, и добавлением её в множество использованных вершин, либо рекурсивный вызов процедуры \textit{extend} с тремя параметрами: уже набранным подграфом, вершинами-кандитами, которые можно присоединить к текущему подграфу, и множеством вершин, которые использовать нельзя. %Последние два, \textit{new\_candidates} и \textit{new\_wrong} соответственно, формируются из соображений.

\subsection*{Пример}
\bigbreak
Далее при описании шагов алгоритма будут использованы следующие цветовые обозначения:\\
ярко-синие вершины\ --\ \textit{new\_candidates},\\
бледно-синие\ --\ \textit{candidates},\\
ярко-красные\ --\ \textit{new\_wrong},\\
бледно-красные\ --\ \textit{wrong},\\
бледно-зеленые\ --\ \textit{compsub},\\
ярко-зеленая\ --\ текущая вершина,\\
серые -- не используемые.\\
"Яркие"\ множества всегда содержатся в соответствующих "бледных".\\

Построено дополнение, состоящее из клик \{0, 1, 2\}, \{0, 3\}, \{2, 4\}, \{3, 7\} и \{4, 5, 6, 7\}. По дополнению построен соответствующий ему исходный граф, по которому восстановлена матрица.

\begin{figure*}[htp]
  \centering
  \subfigure[Исходный граф]{\includegraphics[scale=0.75]{solid.pdf}}\quad
  \subfigure[Дополнение графа]{\includegraphics[scale=0.75]{dashed.pdf}}
\end{figure*}

Полученная матрица 
\[
A = \left(
\begin{array}{cccccccccccccccc}
\Red{1} & \Red{1} & \Red{1} & \Red{1} & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 & \Red{1} & \Red{1} & \Red{1} & \Red{1} & \Red{1} & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & \Red{1} & \Red{1} & \Red{1} & \Red{1} & 0 & 0 & 0 \\
0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & \Red{1} & 0 & 0 & 0 & \Red{1} & \Red{1} & \Red{1} & \Red{1} \\
0 & 0 & 0 & \Red{1} & 0 & 0 & 0 & \Red{1} & 0 & 0 & 0 & 0 & 0 & \Red{1} & 0 & 0 \\
0 & 0 & \Red{1} & 0 & 0 & 0 & \Red{1} & 0 & 0 & 0 & 0 & \Red{1} & 0 & 0 & \Red{1} & 0 \\
0 & \Red{1} & 0 & 0 & 0 & \Red{1} & 0 & 0 & 0 & 0 & \Red{1} & 0 & 0 & 0 & 0 & \Red{1} \\
\Red{1} & 0 & 0 & 0 & \Red{1} & 0 & 0 & 0 & 0 & \Red{1} & 0 & 0 & 0 & 0 & 0 & 0 \\
\end{array}
\right).
\]
Множество обнаруженных алгоритмом клик -- \{0, 1, 2\}, \{0, 3\}, \{2, 4\}, \{3, 7\} и \{4, 5, 6, 7\} -- совпадает с заявленным.

\newpage
\begin{SCfigure}[][!htbp]
  \centering 
  \caption{ 
    Шаг 1\\
    Глубина рекурсии\ :\ 0\\
    \textit{v}\ :\ 0\\
    \textit{compsub}\ :\ \{0\}\\
    \textit{candidates}\ :\ \{0, 1, 2, 3, 4, 5, 6, 7\}\\
    \textit{wrong}\ :\ \{\}\\
    \textit{new\_candidates}\ :\ \{1, 2, 3\}\\
    \textit{new\_wrong}\ :\ \{\}\\
  }
  \includegraphics[scale=0.9]{1.pdf}
\end{SCfigure}
\begin{SCfigure}[][!htbp]
  \centering 
  \caption{ 
    Шаг 2\\
    Глубина рекурсии\ :\ 1\\
    \textit{v}\ :\ 1\\
    \textit{compsub}\ :\ \{0, 1\}\\
    \textit{candidates}\ :\ \{1, 2, 3\}\\
    \textit{wrong}\ :\ \{\}\\
    \textit{new\_candidates}\ :\ \{2\}\\
    \textit{new\_wrong}\ :\ \{\}\\
  }
  \includegraphics[scale=0.9]{2.pdf}
\end{SCfigure}
\begin{SCfigure}[][!htbp]
  \centering 
  \caption{ 
    Шаг 3\\
    Глубина рекурсии\ :\ 2\\
    \textit{v}\ :\ 2\\
    \textit{compsub}\ :\ \{0, 1, 2\}\\
    \textit{candidates}\ :\ \{2\}\\
    \textit{wrong}\ :\ \{\}\\
    \textit{new\_candidates}\ :\ \{\}\\
    \textit{new\_wrong}\ :\ \{\}\\
    Сформированные множества пусты, клика найдена\\
    После удаления 2 из \textit{candidates} это множество становится пусто. Завершение цикла.
  }
  \includegraphics[scale=0.9]{3.pdf}
\end{SCfigure}
\begin{SCfigure}[][!htbp]
  \centering 
  \caption{ 
    Шаг 4\\
    Глубина рекурсии\ :\ 1\\
    \textit{v}\ :\ 2\\
    \textit{compsub}\ :\ \{0, 2\}\\
    \textit{candidates}\ :\ \{2, 3\}\\
    \textit{wrong}\ :\ \{1\}\\
    \textit{new\_candidates}\ :\ \{\}\\
    \textit{new\_wrong}\ :\ \{1\}\\
  }
  \includegraphics[scale=0.9]{4.pdf}
\end{SCfigure}
\begin{SCfigure}[][!htbp]
  \centering 
  \caption{ 
    Шаг 5\\
    Глубина рекурсии\ :\ 1\\
    \textit{v}\ :\ 3\\
    \textit{compsub}\ :\ \{0, 3\}\\
    \textit{candidates}\ :\ \{3\}\\
    \textit{wrong}\ :\ \{1, 2\}\\
    \textit{new\_candidates}\ :\ \{\}\\
    \textit{new\_wrong}\ :\ \{\}\\
    Сформированные множества пусты, клика найдена\\
    После удаления 3 из \textit{candidates} это множество становится пусто. Завершение цикла.
  }
  \includegraphics[scale=0.9]{5.pdf}
\end{SCfigure}
\begin{SCfigure}[][!htbp]
  \centering 
  \caption{ 
    Шаг 6\\
    Глубина рекурсии\ :\ 0\\
    \textit{v}\ :\ 1\\
    \textit{compsub}\ :\ \{1\}\\
    \textit{candidates}\ :\ \{1, 2, 3, 4, 5, 6, 7\}\\
    \textit{wrong}\ :\ \{0\}\\
    \textit{new\_candidates}\ :\ \{2\}\\
    \textit{new\_wrong}\ :\ \{0\}\\
  }
  \includegraphics[scale=0.9]{6.pdf}
\end{SCfigure}
\begin{SCfigure}[][!htbp]
  \centering 
  \caption{ 
    Шаг 7\\
    Глубина рекурсии\ :\ 0\\
    \textit{v}\ :\ 2\\
    \textit{compsub}\ :\ \{2\}\\
    \textit{candidates}\ :\ \{2, 3, 4, 5, 6, 7\}\\
    \textit{wrong}\ :\ \{0, 1\}\\
    \textit{new\_candidates}\ :\ \{4\}\\
    \textit{new\_wrong}\ :\ \{0, 1\}\\
  }
  \includegraphics[scale=0.9]{7.pdf}
\end{SCfigure}
\begin{SCfigure}[][!htbp]
  \centering 
  \caption{ 
    Шаг 8\\
    Глубина рекурсии\ :\ 1\\
    \textit{v}\ :\ 4\\
    \textit{compsub}\ :\ \{2, 4\}\\
    \textit{candidates}\ :\ \{4\}\\
    \textit{wrong}\ :\ \{0, 1\}\\
    \textit{new\_candidates}\ :\ \{\}\\
    \textit{new\_wrong}\ :\ \{\}\\
    Сформированные множества пусты, клика найдена\\
    После удаления 4 из \textit{candidates} это множество становится пусто. Завершение цикла.
  }
  \includegraphics[scale=0.9]{8.pdf}
\end{SCfigure}
\begin{SCfigure}[][!htbp]
  \centering 
  \caption{ 
    Шаг 9\\
    Глубина рекурсии\ :\ 0\\
    \textit{v}\ :\ 3\\
    \textit{compsub}\ :\ \{3\}\\
    \textit{candidates}\ :\ \{3, 4, 5, 6, 7\}\\
    \textit{wrong}\ :\ \{0, 1, 2\}\\
    \textit{new\_candidates}\ :\ \{7\}\\
    \textit{new\_wrong}\ :\ \{0\}\\
  }
  \includegraphics[scale=0.9]{9.pdf}
\end{SCfigure}
\begin{SCfigure}[][!htbp]
  \centering 
  \caption{ 
    Шаг 10\\
    Глубина рекурсии\ :\ 1\\
    \textit{v}\ :\ 7\\
    \textit{compsub}\ :\ \{3, 7\}\\
    \textit{candidates}\ :\ \{7\}\\
    \textit{wrong}\ :\ \{0\}\\
    \textit{new\_candidates}\ :\ \{\}\\
    \textit{new\_wrong}\ :\ \{\}\\
    Сформированные множества пусты, клика найдена\\
    После удаления 7 из \textit{candidates} это множество становится пусто. Завершение цикла.
  }
  \includegraphics[scale=0.9]{10.pdf}
\end{SCfigure}
\begin{SCfigure}[][!htbp]
  \centering
  \caption{ 
    Шаг 11\\
    Глубина рекурсии\ :\ 0\\
    \textit{v}\ :\ 4\\
    \textit{compsub}\ :\ \{4\}\\
    \textit{candidates}\ :\ \{4, 5, 6, 7\}\\
    \textit{wrong}\ :\ \{0, 1, 2, 3\}\\
    \textit{new\_candidates}\ :\ \{5, 6, 7\}\\
    \textit{new\_wrong}\ :\ \{2\}\\
    После удаления 4 из \textit{candidates} и добавления её в \textit{wrong}\\
    \textit{wrong} = \{0, 1, 2, 3, 4\}\\
    \textit{candidates} = \{5, 6, 7\}\\
    Вершина 4 из \textit{wrong} не соединена ни с одной из вершин из \textit{candidates}. Завершение цикла.
  }
  \includegraphics[scale=0.9]{11.pdf}
\end{SCfigure}
\begin{SCfigure}[][!htbp]
  \centering
  \caption{ 
    Шаг 12\\
    Глубина рекурсии\ :\ 1\\
    \textit{v}\ :\ 5\\
    \textit{compsub}\ :\ \{4, 5\}\\
    \textit{candidates}\ :\ \{5, 6, 7\}\\
    \textit{wrong}\ :\ \{2\}\\
    \textit{new\_candidates}\ :\ \{6, 7\}\\
    \textit{new\_wrong}\ :\ \{\}\\
    После удаления 5 из \textit{candidates} и добавления её в \textit{wrong}\\
    \textit{wrong} = \{2, 5\}\\
    \textit{candidates} = \{6, 7\}\\
    Вершина 5 из \textit{wrong} не соединена ни с одной вершиной из \textit{candidates}. Завершение цикла.
  }
  \includegraphics[scale=0.9]{12.pdf}
\end{SCfigure}
\begin{SCfigure}[][!htbp]
  \centering
  \caption{
    Шаг 13\\
    Глубина рекурсии\ :\ 2\\
    \textit{v}\ :\ 6\\
    \textit{compsub}\ :\ \{4, 5, 6\}\\
    \textit{candidates}\ :\ \{6, 7\}\\
    \textit{wrong}\ :\ \{\}\\
    \textit{new\_candidates}\ :\ \{7\}\\
    \textit{new\_wrong}\ :\ \{\}\\
    После удаления 6 из \textit{candidates} и добавления её в \textit{wrong}\\
    \textit{wrong} = \{6\}\\
    \textit{candidates} = \{7\}\\
    Вершина 6 из \textit{wrong} не соединена с единственной вершиной 7 из \textit{candidates}. Завершение цикла.
  }
  \includegraphics[scale=0.9]{13.pdf}
\end{SCfigure}
\begin{SCfigure}[][!htbp]
  \centering
  \caption{ 
    Шаг 14\\
    Глубина рекурсии\ :\ 3\\
    \textit{v}\ :\ 7\\
    \textit{compsub}\ :\ \{4, 5, 6, 7\}\\
    \textit{candidates}\ :\ \{7\}\\
    \textit{wrong}\ :\ \{\}\\
    \textit{new\_candidates}\ :\ \{\}\\
    \textit{new\_wrong}\ :\ \{\}\\
    Сформированные множества пусты, клика найдена\\
    После удаления 7 из \textit{candidates} это множество становится пусто. Завершение цикла.
  }
  \includegraphics[scale=0.9]{14.pdf}
\end{SCfigure}

\newpage
\subsection*{Результаты}
\bigbreak
\begin{enumerate}  
\item Графы c равными степенями вершин (K Regular).
\item Модель Эрдюсома и Рени, все графы на фиксированном множестве вершин с фиксированным числом ребер одинаково вероятны. (Erdos Renyi)
\item 
Модель Барабаши — Альберт моделей со степенным распределением, которые генерируют сети. Она включает в себя две концепции: рост сети, принцип предпочтительного присоединения. Принцип предпочтительного присоединения заключается в том, что чем больше связей имеет узел, тем более предпочтительно для него создание новых связей. Узлы с наибольшей степенью имеют больше возможностей забирать себе связи, добавляемые в сеть. (Barabasi Albert)
\end{enumerate}
\bigbreak
Результаты работы алгоритма на разном количестве вершин и ребер приведены в следующей таблице:
\bigbreak
\begin{center}
  \begin{tabular}{| c | c | c | c | c | c |}
    \hline
    Вершины & Рёбра & Тип граф & Среднее время & Мин. время & Макс время   \\ \hline
    10 & 10 & Erdos Renyi & 0.0001 & 0.0001 & 0.0001\\ \hline 
    10 & 25 & K Regular & 0.0001 & 0.0 & 0.0001\\ \hline 
    50 & 50 & Erdos Renyi & 1.5237 & 1.3222 & 1.7857\\ \hline 
    50 & 50 & Barabasi Albert & 0.0005 & 0.0005 & 0.0006\\ \hline 
    50 & 50 & K Regular & Timeout &  -  &  - \\ \hline 
    50 & 100 & Erdos Renyi & 5.4053 & 3.2634 & 8.2774\\ \hline 
    50 & 100 & Barabasi Albert & 0.0008 & 0.0008 & 0.0008\\ \hline 
    50 & 125 & K Regular & 6.6286 & 5.6114 & 7.4728\\ \hline 
    50 & 200 & Erdos Renyi & 1.1868 & 1.1315 & 1.2392\\ \hline 
    50 & 200 & Barabasi Albert & 0.0007 & 0.0007 & 0.0007\\ \hline 
    50 & 250 & K Regular & 0.7941 & 0.7821 & 0.8118\\ \hline 
    50 & 500 & Erdos Renyi & 0.0523 & 0.0496 & 0.0542\\ \hline 
    50 & 500 & Barabasi Albert & 0.0005 & 0.0005 & 0.0005\\ \hline 
    50 & 625 & K Regular & 0.0168 & 0.0156 & 0.0178\\ \hline 
    50 & 612 & Erdos Renyi & 0.0207 & 0.0199 & 0.0213\\ \hline 
    50 & 612 & Barabasi Albert & 0.0005 & 0.0005 & 0.0006\\ \hline 
    100 & 600 & K Regular & Timeout &  -  &  - \\ \hline 
    100 & 100 & Erdos Renyi & Timeout &  -  &  - \\ \hline 
    100 & 100 & Barabasi Albert & 0.0032 & 0.003 & 0.0034\\ \hline 
    100 & 100 & K Regular & Timeout &  -  &  - \\ \hline 
    100 & 500 & Erdos Renyi & Timeout &  -  &  - \\ \hline 
    100 & 500 & Barabasi Albert & 0.0017 & 0.0016 & 0.0018\\ \hline 
    100 & 500 & K Regular & Timeout &  -  &  - \\ \hline 
    100 & 2000 & Erdos Renyi & 2.8622 & 2.6434 & 3.0496\\ \hline 
    100 & 2000 & Barabasi Albert & 0.0014 & 0.0014 & 0.0014\\ \hline 
    100 & 2000 & K Regular & 1.4036 & 1.2942 & 1.5998\\ \hline 
    100 & 4900 & Erdos Renyi & 0.0015 & 0.0015 & 0.0015\\ \hline 
    100 & 4900 & Barabasi Albert & 0.0013 & 0.0013 & 0.0013\\ \hline 
    100 & 4500 & K Regular & 0.003 & 0.0028 & 0.0031\\ \hline 
    1000 & 1000 & Erdos Renyi & Timeout &  -  &  - \\ \hline 
    1000 & 1000 & Barabasi Albert & 0.1893 & 0.1773 & 0.2011\\ \hline 
    1000 & 1000 & K Regular & Timeout &  -  &  - \\ \hline 
    1000 & 5000 & Erdos Renyi & Timeout &  -  &  - \\ \hline 
    1000 & 5000 & Barabasi Albert & 0.4703 & 0.4156 & 0.5162\\ \hline 
    1000 & 5000 & K Regular & Timeout &  -  &  - \\ \hline 
    1000 & 10000 & Erdos Renyi & Timeout &  -  &  - \\ \hline 
    1000 & 10000 & Barabasi Albert & 0.3424 & 0.1463 & 0.7211\\ \hline 
    1000 & 10000 & K Regular & Timeout &  -  &  - \\ \hline 
    1000 & 100000 & Erdos Renyi & Timeout &  -  &  - \\ \hline 
    1000 & 100000 & Barabasi Albert & 0.1782 & 0.1749 & 0.1801\\ \hline 
    1000 & 100000 & K Regular & Timeout &  -  &  - \\ \hline 
    1000 & 497500 & Erdos Renyi & 0.2459 & 0.2325 & 0.2579\\ \hline 
    1000 & 497500 & Barabasi Albert & 0.1514 & 0.1237 & 0.1965\\ \hline 
    1000 & 495500 & Barabasi Albert & 0.1427 & 0.1175 & 0.192\\ \hline   
  \end{tabular}
\end{center}
    

\bigbreak
\subsection*{Заключение}

\bigbreak
В ходе работы был реализован алгоритм Брона — Кербоша, и проведен анализ его работы.

\bigbreak
Алгоритм Брона — Кербоша является одним из самых эффективных алгоритмов поиска клик (независимых множеств) в графах. Однако асимптотика $O(3^{\frac{N}{3}})$ не позволяет его использовать на произвольных больших графах. Для графов порядка 50 вершин, время работы не превышает 5 секунд. Исследование показало, что для больших графов(порядка 1000 вершин) в случае сильно разреженных или сильно плотных графов алгоритм успевает завершить свою работу за разумное время.

\bigbreak
Цель иследовательской работы была успешно достигнута, алгоритм Брона — Кербоша применён для решения задачи разбиения системы линейных неравенств на максимальные группы неравенств не содержащих общих переменных.



\begin{thebibliography}{}
    \bibitem{litlink1}
    \href{https://en.wikipedia.org/wiki/Bron\%E2\%80\%93Kerbosch_algorithm}{en.wikipedia.org/wiki/Bron--Kerbosch\_algorithm}
    \bibitem{description}
    \href{https://ru.wikipedia.org/wiki/\%D0\%90\%D0\%BB\%D0\%B3\%D0\%BE\%D1\%80\%D0\%B8\%D1\%82\%D0\%BC_\%D0\%91\%D1\%80\%D0\%BE\%D0\%BD\%D0\%B0_\%E2\%80\%94_\%D0\%9A\%D0\%B5\%D1\%80\%D0\%B1\%D0\%BE\%D1\%88\%D0\%B0}{ru.wikipedia.org/wiki/Описание\_Алгоритма\_Брона\_—\_Кербоша}
    
    \bibitem{implementation}
    \href{https://ru.wikibooks.org/wiki/\%D0\%A0\%D0\%B5\%D0\%B0\%D0\%BB\%D0\%B8\%D0\%B7\%D0\%B0\%D1\%86\%D0\%B8\%D0\%B8_\%D0\%B0\%D0\%BB\%D0\%B3\%D0\%BE\%D1\%80\%D0\%B8\%D1\%82\%D0\%BC\%D0\%BE\%D0\%B2/\%D0\%90\%D0\%BB\%D0\%B3\%D0\%BE\%D1\%80\%D0\%B8\%D1\%82\%D0\%BC_\%D0\%91\%D1\%80\%D0\%BE\%D0\%BD\%D0\%B0_\%E2\%80\%94_\%D0\%9A\%D0\%B5\%D1\%80\%D0\%B1\%D0\%BE\%D1\%88\%D0\%B0}{ru.wikibooks.org/wiki/Реализации\_алгоритмов/Алгоритм\_Брона\_—\_Кербоша}
    
\end{thebibliography}

\newpage
\paragraph{Приложения}
@o run.py @{from subprocess import call
call(["mkdir", "src"])
call(["mkdir", "tests"])
call(["mkdir", "graphs"])
call(["nuweb", "report.w"])
call(["pdflatex", "--shell-escape", "report.tex"])
# call(["python3", "run_tests.py"])
# call(["python3", "gen_sample.py"])
@}

@d Define Erdos Renyi graph gen func @{def gen_Erdos_Renyi_graph(N, M):
  global cnt
  for i in range(3):
    cnt += 1
    name = "test" + str(cnt) + ".txt" 
    f = open(name, 'w')
    f.write("Erdos_Renyi\n")
    f.write(str(N) + " " + str(M) + "\n")
    g = igraph.Graph.Erdos_Renyi(n=N, m=M)
    g.write_edgelist(f)  
@}

@d Define K-Regular graph gen func @{def gen_K_Regular_graph(N, K):
  global cnt
  for i in range(3):
    cnt += 1
    name = "test" + str(cnt) + ".txt" 
    f = open(name, 'w')
    f.write("K_Regular\n")
    f.write(str(N) + " " + str(N * K / 2) + "\n")
    g = igraph.Graph.K_Regular(n=N, k=K)
    g.write_edgelist(f)
@}

@d Define Barabasi graph gen func @{def gen_Barabasi_graph(N, M):
  global cnt
  for i in range(3):
    cnt += 1
    name = "test" + str(cnt) + ".txt" 
    f = open(name, 'w')
    f.write("Barabasi_Albert\n")
    f.write(str(N) + " " + str(M) + "\n")
    g = igraph.Graph.Barabasi(n=N, m=M)
    g.write_edgelist(f)
@}

@d Generate Erdos Renyi graphs@{gen_Erdos_Renyi_graph(10, 10)
gen_Erdos_Renyi_graph(10, 30)
gen_Erdos_Renyi_graph(10, 10 * 9 / 2 - 5)
gen_Erdos_Renyi_graph(50, 50)
gen_Erdos_Renyi_graph(50, 100)
gen_Erdos_Renyi_graph(50, 200)
gen_Erdos_Renyi_graph(50, 500)
gen_Erdos_Renyi_graph(50, 50 * 49 / 2 / 2)
gen_Erdos_Renyi_graph(100, 100)
gen_Erdos_Renyi_graph(100, 500)
gen_Erdos_Renyi_graph(100, 2000)
gen_Erdos_Renyi_graph(100, 100 * 99 / 2 - 50)
gen_Erdos_Renyi_graph(1000, 1000)
gen_Erdos_Renyi_graph(1000, 5000)
gen_Erdos_Renyi_graph(1000, 10000)
gen_Erdos_Renyi_graph(1000, 100000)
gen_Erdos_Renyi_graph(1000, 1000 * 999 / 2 - 2000) 
@}
@d Generate Barabasi graphs@{gen_Barabasi_graph(10, 10)
gen_Barabasi_graph(10, 30)
gen_Barabasi_graph(10, 10 * 9 / 2 - 5)
gen_Barabasi_graph(50, 50)
gen_Barabasi_graph(50, 100)
gen_Barabasi_graph(50, 200)
gen_Barabasi_graph(50, 500)
gen_Barabasi_graph(50, 50 * 49 / 2 / 2)
gen_Barabasi_graph(100, 100)
gen_Barabasi_graph(100, 500)
gen_Barabasi_graph(100, 2000)
gen_Barabasi_graph(100, 100 * 99 / 2 - 50)
gen_Barabasi_graph(1000, 1000)
gen_Barabasi_graph(1000, 5000)
gen_Barabasi_graph(1000, 10000)
gen_Barabasi_graph(1000, 100000)
gen_Barabasi_graph(1000, 1000 * 999 / 2 - 2000)
@}
@d Generate K-Regular graphs @{gen_K_Regular_graph(10, 2)
gen_K_Regular_graph(10, 5)
gen_K_Regular_graph(10, 8)
gen_K_Regular_graph(50, 2)
gen_K_Regular_graph(50, 5)
gen_K_Regular_graph(50, 10)
gen_K_Regular_graph(50, 25)
gen_K_Regular_graph(100, 12)
gen_K_Regular_graph(100, 2)
gen_K_Regular_graph(100, 10)
gen_K_Regular_graph(100, 40)
gen_K_Regular_graph(100, 90)
gen_K_Regular_graph(1000, 2)
gen_K_Regular_graph(1000, 10)
gen_K_Regular_graph(1000, 20)
gen_K_Regular_graph(1000, 200)
gen_K_Regular_graph(1000, 1000 * 999 / 2 - 4000)
@}
@o tests/gen.py @{import igraph
cnt = 0
@< Define Erdos Renyi graph gen func@>
@< Define K-Regular graph gen func@>
@< Define Barabasi graph gen func@>
@< Generate Erdos Renyi graphs@>
@< Generate Barabasi graphs@>
@< Generate K-Regular graphs@>
@}

@o run_tests.py @{
import time
import argparse
import signal
import sys

from src.source import bron_kerbosch_max_by_inclusion

@<timeout@>
@<read graph@>

sys.setrecursionlimit(30000)

M, N, GRAPH_TYPE = 0, 0, ""

parser = argparse.ArgumentParser()
parser.add_argument("-t", "--timeout", type=int)
args = parser.parse_args()

for i in range(153 // 9):
    sum_t = 0
    max_t = 0
    min_t = 1000
    Type_max = ""
    Type_min = ""
    cnt = 0
    for j in range(9):
        file_name = "tests/test" + str(i * 9 + 1 + j) + ".txt"
        g = read_graph(file_name)
        start = time.time()

        if (args.timeout is not None):
            timeout(bron_kerbosch_max_by_inclusion, args.timeout, [g])
        else:
            bron_kerbosch_max_by_inclusion(g)

        cnt += 1
        sum_t +=  time.time() - start
        if (max_t < time.time() - start):
            Type_max = GRAPH_TYPE
        max_t = max(time.time() - start, max_t)
        if (min_t > time.time() - start):
            Type_min = GRAPH_TYPE
        min_t = min(time.time() - start, min_t)

    print("Vertices: " + str(N) + " ")
    print("Edges: " + str(M) + "\n")
    print("Average time: " + str(round(sum_t / cnt, 5)) + "\n")
    print("Max time: " + str(round(max_t, 5)) + " Test type:" + Type_max)
    print("Min time: " + str(round(min_t / 9, 5)) + " Test type:" + Type_min)

@}

@d read graph @{
def read_graph(file_name):
    f = open(file_name, 'r')
    global M, N, GRAPH_TYPE
    GRAPH_TYPE = f.readline()
    S = f.readline()
    n, m = map(int, S.split(' '))
    N = n
    M = m
    result = [[False for j in range(n)] for i in range(n)]
    for j in range(m):
        S = f.readline()
        a, b = map(int, S.split(' '))
        result[a][b] = result[b][a] = True
    return result
@}

@d timeout @{
def timeout(func, timeout, args=(), kwargs={}):

    class TimeoutError(Exception):
        pass

    def handler(signum, frame):
        raise TimeoutError()

    signal.signal(signal.SIGALRM, handler)
    signal.alarm(timeout)
    try:
        result = func(*args, **kwargs)
    except TimeoutError as exc:
        print("Timeout has expired")
        result = -1
    finally:
        signal.alarm(0)

    return result
@}

@o gen_sample.py @{
import re
import subprocess

timer = 0
gv_file_prefix = "graph prof {\n\
ratio = fill;\n\
node [style=filled];\n"

def bron_kerbosch_max_by_inclusion(m):
    results = []

    def check(candidates, wrong):
        for i in wrong:
            q = True
            for j in candidates:
                if m[i][j]:
                    q = False
                    break
            if q:
                return False
        return True

@<extend@>
@<make graph@>

subprocess.call(["mkdir", "grafs"])
rawinput = "\
a b c d - - - - - - - - - - - -\n\
- - - - e f g h i - - - - - - -\n\
- - - - - - - - - j k l m - - -\n\
- - - - - - - - i - - - m n o p\n\
- - - d - - - h - - - - - n - -\n\
- - c - - - g - - - - l - - o -\n\
- b - - - f - - - - k - - - - p\n\
a - - - e - - - - j - - - - - -"
rawinput = re.sub("[a-z]", "1", rawinput)
rawinput = re.sub("-", "0", rawinput)
mat = list(map(lambda s: list(map(int, s.split())), rawinput.split('\n')))
g = make_graph(mat)
print(bron_kerbosch_max_by_inclusion(g))
@}

@d extend @{
    def extend(compsub, candidates, wrong, depth=0):
        global timer
        global gv_file_prefix
        while candidates and check(candidates, wrong):
            current_step_graph = gv_file_prefix
            timer += 1
            print("Running step {}".format(timer))
            v = candidates[0]
            compsub.append(v)
            new_candidates = [i for i in candidates if not m[i][v] and i != v]
            new_wrong = [i for i in wrong if not m[i][v] and i != v]
            for i in range(8):
                hue, sat, val = "0.6", "0.1", "0.8"
                if i in candidates:
                    hue, sat, val = "0.6", "0.4", "1.0"
                if i in compsub:
                    hue, sat, val = "0.4", "0.4", "1.0"
                if i in wrong:
                    hue, sat, val = "0.0", "0.4", "1.0"
                if i in new_candidates:
                    hue, sat, val = "0.6", "0.9", "1.0"
                if i in new_wrong:
                    hue, sat, val = "0.0", "0.9", "1.0"
                if i == v:
                    hue, sat, val = "0.4", "0.9", "1.0"
                current_step_graph += "{} [color=\"{} {} {}\"];\n".format(i, hue, sat, val)
            current_step_graph += "}\n"
            filename = "grafs/{}".format(str(timer))
            open(filename + ".gv", "w").write(current_step_graph)
            subprocess.call(["fdp", "-Tps2", filename + ".gv", "-o", filename + ".ps"])
            subprocess.call(["ps2pdf", filename + ".ps", filename + ".pdf"])
            subprocess.call(["rm", filename + ".ps"])
            subprocess.call(["rm", filename + ".gv"])
            if not new_candidates and not new_wrong:
                results.append(list(compsub))
            else:
                extend(compsub, new_candidates, new_wrong, depth+1)
            candidates.remove(v)
            compsub.remove(v)
            wrong.append(v)

    extend([], list(range(len(m))), [])
    return results
@}

@d make graph @{
def make_graph(mat):
    global gv_file_prefix
    n = len(mat)
    m = len(mat[0])
    result = [[False for j in range(n)] for i in range(n)]
    for j in range(m):
        adj = []
        for i in range(n):
            if mat[i][j] != 0:
                for k in adj:
                    result[i][k] = result[k][i] = True
                adj.append(i)
    for i in range(n):
        for j in range(n):
            if j >= i:
                break
            if not result[i][j]:
                gv_file_prefix += '{} -- {} [color="0.650 0.700 0.700" style=dashed];\n'.format(j, i)
    return result

@}

\end{document}
